var app = angular.module('taskApp', ['ngRoute', 'taskApp.controllers', 'taskApp.services'], function($interpolateProvider) {
	$interpolateProvider.startSymbol('<%');
	$interpolateProvider.endSymbol('%>');
});