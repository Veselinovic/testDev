var app = angular.module('taskApp.controllers', []);



app.controller('tasksController', function($scope, $http, Task) {

    $scope.priorities = [{val: 1},{val: 2},{val: 3},{val: 4},{val: 5}];
    $scope.task = new Task();
    $scope.errors = {};
    $scope.changeAddButton = false;

    $scope.init = function(){
        $scope.tasks = Task.query();
    }  

    $scope.addTask = function(){

        Task.store($scope.task)
        .$promise.then(function(response){
            $scope.task = response;
            $scope.tasks.push($scope.task);
            $scope.task = new Task();
            $scope.errors = {};
        },
        function(response){
          $scope.errors = response.data; 

      });

    };

    $scope.updateTask = function(task) {
        Task.update({ id: task.id }, task);
    };

    $scope.deleteTask = function(task) {
        Task.delete({id: task.id }, task)
        .$promise.then(function() {
            var index = $scope.tasks.indexOf(task);
            $scope.tasks.splice(index, 1);
            $scope.loading = false;
            $scope.changeAddButton = false;
            $scope.task = {};
        });
    };

    $scope.editTask = function(task) {
      $scope.task = task;
      $scope.changeAddButton = true;
    }

    $scope.saveChangesAfterEdit = function(){
     Task.update({ id: $scope.task.id }, $scope.task)
    .$promise.then(function(){
        $scope.changeAddButton = false;
        $scope.errors = {};
        $scope.task=new Task();
    },
     function(response){
     $scope.errors = response.data;
     });                            
    
    }


$scope.init();

});
