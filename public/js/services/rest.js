var tasksRest = angular.module('taskApp.services', ['ngResource']);

tasksRest.factory('Task', ['$resource',
	function($resource){
		return $resource('api/tasks/:idTask', {idTask: '@id'}, {
			'query': {method:'GET', params:{}, isArray:true},
			'delete': { method: 'DELETE'},
			'store': { method: 'POST' },
			'update': {method: 'PUT'}
		});
	}]);