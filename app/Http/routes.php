<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(array('middleware' => 'guest',['except' => 'logout']), function () {
	Route::get('register', [ 'as' => 'register_page', 'uses' => 'Auth\RegistrationController@register']);
	Route::post('register', [ 'as' => 'register_page_post', 'uses' => 'Auth\RegistrationController@postRegister']);
	Route::get('login', [ 'as' => 'login_page', 'uses' => 'Auth\LoginController@login']);
	Route::post('login', [ 'as' => 'login_page_post', 'uses' => 'Auth\LoginController@postLogin']);
});

Route::get('logout', [ 'as' => 'logout_page', 'uses' => 'Auth\LoginController@logout']);

Route::controllers([
	'password' => 'Auth\PasswordController',
	]);


Route::group(array('middleware' => 'auth'), function () {
	Route::resource('api/tasks','TasksController');
	Route::get('/','PagesController@showTasksView');
	Route::get('/tasks','PagesController@showTasksView');
});





