@extends('layouts.master')
@section('content')
<br/>
<h1>TODO list</h1>
<div class="container" ng-app="taskApp" ng-controller="tasksController">
  <h1>Manage todo</h1>
  <form class="form-inline row" role="form">
    <div class="form-group col-md-3">
      <label class="sr-only" for="inputName">Name</label>
      <input type="text" class="form-control" id="inputName"  ng-model="task.name" placeholder="name">
    </div>
    <div class="form-group col-md-3">
      <label class="sr-only" >Description</label>
      <input type="text" class="form-control"  ng-model="task.description" placeholder="description">
    </div>
    <div class="form-group col-md-3">
      <label>
        <select class="form-control" ng-model="task.priority">
          <option  ng-repeat="(key, val) in priorities"><% ::val.val %></option>
        </select>
      </label>
    </div>
    <div class="col-md-3">
      <button class="btn btn-primary btn-md" ng-show="changeAddButton == false;"  ng-click="addTask();">Add</button>
      <button class="btn btn-danger btn-md" ng-show="changeAddButton == true;"  ng-click="saveChangesAfterEdit();">Change</button>
    </div>

  </form>
  <form class="form-inline row" role="form">
    <div class="form-group col-md-3">
      <label class="text-danger" ng-hide="if(errors.name[0] == false);"><% errors.name[0] %> </label>
    </div>
    <div class="form-group col-md-3">
     <label class="text-danger"  ng-hide="if(errors.description[0] == false);"><% errors.description[0] %> </label>
    </div>
    <div class="form-group col-md-3">
     <label class="text-danger"  ng-hide="if(errors.priority[0] == false);"><% errors.priority[0] %> </label>
    </div>

  </form>
  <i ng-show="loading" class="fa fa-spinner fa-spin"></i>

  <hr>
  <div class="row">
    <div class="col-md-4">
      <table class="table table-striped">
        <tr>
          <td class="col-md-4">Done</td>
          <td class="col-md-4">Name</td>
          <td class="col-md-4">Description</td>
          <td class="col-md-4">Priority</td>
          <td class="col-md-4" colspan="2">Actions</td>
        </tr>
        <tr ng-repeat='task in tasks | orderBy:"-priority"'>
          <td><input type="checkbox" ng-checked="task.done == 1" ng-model="task.done" ng-change="updateTask(task)"></td>
          <td><% task.name %></td>
          <td><% task.description %></td>
          <td><label><select  ng-model='task.priority' ng-change="updateTask(task)">
            <option ng-repeat="(key, val) in priorities"><% ::val.val %></option>
          </select></label></td>
          <td><button class="btn btn-danger btn-xs" ng-click="deleteTask(task, index)">  <span class="glyphicon glyphicon-trash" ></span></button></td>
          <td><button class="btn btn-danger btn-xs" ng-click="editTask(task)">Edit</button></td>
        </tr>
      </table>
    </div>
  </div>
</div>
@endsection
@section('scripts')
{!! Html::script('js/lib/angular/angular.js') !!}
{!! Html::script('js/lib/angular/angular-route.js') !!}
{!! Html::script('js/lib/angular/angular-resource.min.js') !!}
{!! Html::script('js/services/rest.js') !!}
{!! Html::script('js/controllers/TaskListController.js') !!}
{!! Html::script('js/app.js') !!}
@endSection


