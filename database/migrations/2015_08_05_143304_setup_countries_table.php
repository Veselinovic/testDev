<?php

use Illuminate\Database\Migrations\Migration;

class SetupCountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return  void
	 */
	public function up()
	{
		// Creates the users table
		Schema::create('countries', function($table)
		{		    
		    $table->increments('id');
		    $table->string('country_code', 3)->default('');
		    $table->string('name', 255)->default('');

		   
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return  void
	 */
	public function down()
	{
		Schema::drop('countries');
	}

}
