<?php

use Illuminate\Database\Seeder;

class CountriesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        //Get all of the countries       
        $countries = json_decode(file_get_contents('./database/seeds/data/countries.json'), true);
        foreach ($countries as $countryId => $country){
            DB::table('countries')->insert(array(
                'country_code' => $country['country-code'],
                'name' => $country['name'],
            ));
        }
    }
}
